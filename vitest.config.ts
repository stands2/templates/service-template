import { defineConfig } from 'vitest/config';

const { CI } = process.env;

export default defineConfig({
  test: {
    environment: 'node',
    ui: CI ? false : true,
    reporters: CI ? ['default', 'junit'] : ['default'],
    outputFile: CI ? { junit: 'junit.xml' } : {},
    coverage: {
      enabled: CI ? true : false,
      reporter: CI ? ['text', 'cobertura', 'html'] : ['text'],
      lines: 80,
    },
  },
});
